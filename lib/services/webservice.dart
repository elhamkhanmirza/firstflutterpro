
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class Resource<T> {
  final String url; 
  T Function(Response response) parse;

  Resource({this.url,this.parse});
}

class Webservice {

  Future<T> user<T>(Resource<T> resource) async {

      final response = await http.get(resource.url+"/user");
      if(response.statusCode == 200) {
        return resource.parse(response);
      } else {
        throw Exception('Failed to load data!');
      }
  }

  Future<T> userDetail<T>(Resource<T> resource,String contactId) async {

    final response = await http.get(resource.url+"/user/"+contactId);
    if(response.statusCode == 200) {
      return resource.parse(response);
    } else {
      throw Exception('Failed to load data!');
    }
  }

}