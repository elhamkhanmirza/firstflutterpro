import 'dart:convert';
import 'package:herokuapp/services/webservice.dart';
import 'package:herokuapp/utils/constants.dart';

class Contact {
  final String id;
  final String first_name;
  final String last_name;
  final String email;
  final String gender;
  final String date_of_birth;
  final String phone_no;

  Contact(
      {this.id,
      this.first_name,
      this.last_name,
      this.email,
      this.gender,
      this.date_of_birth,
      this.phone_no});

  factory Contact.fromJson(Map<String, dynamic> json) {
    return Contact(
      id: json['id'],
      first_name: json['first_name'],
      last_name: json['last_name'],
      email: json['email'],
      gender: json['gender'],
      date_of_birth: json['date_of_birth'],
      phone_no: json['phone_no'],
    );
  }

  static Resource<List<Contact>> get all {
    return Resource(
        url: Constants.BASE_URL,
        parse: (response) {
          final result = json.decode(response.body);
          Iterable list = result['data'];
          return list.map((model) => Contact.fromJson(model)).toList();
        });
  }

  static Resource<Contact> get detail {
    return Resource(
        url: Constants.BASE_URL,
        parse: (response) {
          final result = json.decode(response.body);
          Contact contact=Contact.fromJson(result['data'][0]);
          return contact;
        });
  }
}
