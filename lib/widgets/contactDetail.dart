import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:herokuapp/models/contact.dart';

import '../services/webservice.dart';

class ContactDetailState extends State<ContactDetailPage> {
  String contactId;

  Contact _contact = null;

  ContactDetailState(contactId) {
    this.contactId = contactId;
  }

  @override
  void initState() {
    super.initState();
    _getContact();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contact Detial"),
      ),
      body: ListView(
          children: [
            imageSection,
            nameSection(),
            emailSection(),
            genderSection(),
            new Divider(color:  Colors.black, indent: 20, endIndent: 20),
            numberSection(),
            iconSection,
            new Divider(color:  Colors.black, indent: 20, endIndent: 20),

            ]


      ),
    );
  }


  Widget numberSection() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//     mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 40, top: 16),
          child: Text(
            "Mobile",
            textAlign: TextAlign.left,
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: 40, top: 16),
          child: Text(
            _contact == null ? "" : _contact.phone_no,
            textDirection: TextDirection.ltr,
            textAlign: TextAlign.left,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }

  Widget iconSection = Row(
    mainAxisAlignment: MainAxisAlignment.end,
//    mainAxisSize: MainAxisSize.min,
    children: <Widget>[
      Container(
        margin: EdgeInsets.only(right: 16, top: 16, bottom: 16),
        child: Icon(Icons.phone),
      ),
      Container(
        margin: EdgeInsets.only(right: 50, top: 16, bottom: 16),
        child: Icon(Icons.chat_bubble_outline),
      )
    ],
  );

  Widget imageSection = Container(
    padding: const EdgeInsets.all(16),
    width: 200,
    height: 200,
    child: CircleAvatar(
    backgroundColor: Colors.grey,
    ),
  );

  Widget nameSection(){
    return Container(
      child: Text(
          _contact == null ? "" : _contact.first_name + " " + _contact.last_name,
          softWrap: true,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
      ),
    );
  }

  Widget emailSection() {
    return Container(
      margin: const EdgeInsets.only(top: 16),
      child: Text(
          _contact == null ? "" : _contact.email,
          softWrap: true,
          textAlign: TextAlign.center,
          style: TextStyle(fontStyle: FontStyle.italic, fontSize: 15),
      ),
    );
  }

    Widget genderSection() {
      return Container(
        margin: const EdgeInsets.only(top: 16, bottom: 16),
        child: Text(
            _contact == null ? "" : _contact.gender,
            softWrap: true,
            textAlign: TextAlign.center,
            style: TextStyle(fontStyle: FontStyle.italic, fontSize: 15),
        ),
      );
    }

    void _getContact() {

      Webservice().userDetail(Contact.detail,contactId).then((contact) => {
        setState(() => {
          _contact = contact
        })
      });

    }
}


class ContactDetailPage extends StatefulWidget {
  String contactId;

  ContactDetailPage(contactId) {
    this.contactId = contactId;
  }

  @override
  createState() => ContactDetailState(contactId);
}
