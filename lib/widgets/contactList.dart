import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:herokuapp/models/contact.dart';
import 'package:herokuapp/services/webservice.dart';

import 'contactDetail.dart';

class ContactListState extends State<ContactListPage> {

  List<Contact> _contacts = List<Contact>();
  final _biggerFont = const TextStyle(fontSize: 18.0);

  @override
  void initState() {
    super.initState();
    _getContact();
  }

  void _getContact() {

    Webservice().user(Contact.all).then((contacts) => {
      setState(() => {
        _contacts = contacts
      })
    });

  }

  ListTile _buildItemsForListView(BuildContext context, int index) {
    return new ListTile(
      leading: new CircleAvatar(
        backgroundColor: Colors.grey,
//        backgroundImage: new NetworkImage(_newsArticles[index].profileImageUrl),
      ),
      title: new Text(_contacts[index].first_name+" "+_contacts[index].last_name,
        style: _biggerFont,
      ),
      subtitle: new Text(_contacts[index].email),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ContactDetailPage(_contacts[index].id)));
      },
    );

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('News'),
        ),
        body: ListView.builder(
          itemCount: _contacts.length,
//          itemBuilder: _buildItemsForListView,
          itemBuilder:(context, i)
          {
            if (i.isOdd) return new Divider();
            return _buildItemsForListView(context, i);
          },

        )
    );
  }





}

class ContactListPage extends StatefulWidget {

  @override
  createState() => ContactListState();
}